﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using Homework.Interfaces;

namespace Homework.Models
{
    public class JpegDocument : IDocument
    {
        public void Open()
        {
            MessageBox.Show("Open .jpg/.jpeg");
        }

        public void Save()
        {
            MessageBox.Show("Save .jpg/.jpeg");
        }

        public void SaveAs()
        {
            MessageBox.Show("Save as .jpg/.jpeg");
        }

        public void Print()
        {
            MessageBox.Show("Print .jpg/.jpeg");
        }

        public void Close()
        {
            MessageBox.Show("Close .jpg/.jpeg");
        }

        public void Create()
        {
            MessageBox.Show("Create .jpg/.jpeg");
        }
    }
}
