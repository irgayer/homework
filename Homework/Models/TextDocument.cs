﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using Homework.Interfaces;

namespace Homework.Models
{
    public class TextDocument : IDocument
    {
        public void Open()
        {
            MessageBox.Show("Open .txt");
        }

        public void Save()
        {
            MessageBox.Show("Save .txt");
        }

        public void SaveAs()
        {
            MessageBox.Show("Save as .txt");
        }

        public void Print()
        {
            MessageBox.Show("Print .txt");
        }

        public void Close()
        {
            MessageBox.Show("Close .txt");
        }

        public void Create()
        {
            MessageBox.Show("Create .txt");
        }
    }
}
