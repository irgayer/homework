﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using Homework.Interfaces;

namespace Homework.Models
{
    public class PngDocument : IDocument
    {
        public void Open()
        {
            MessageBox.Show("Open .png");
        }

        public void Save()
        {
            MessageBox.Show("Save .png");
        }

        public void SaveAs()
        {
            MessageBox.Show("Save as .png");
        }

        public void Print()
        {
            MessageBox.Show("Print .png");
        }

        public void Close()
        {
            MessageBox.Show("Close .png");
        }

        public void Create()
        {
            MessageBox.Show("Create .png");
        }
    }
}
