﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Homework.Interfaces;

namespace Homework
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DocumentFactory factory;
        IDocument document;
        string fileType = "JPEG";
        public MainWindow()
        {
            InitializeComponent();
            factory = new DocumentFactory();
        }

        void CreateButtonClick(object sender, RoutedEventArgs e)
        {
           document?.Create();
        }

        void OpenButtonClick(object sender, RoutedEventArgs e)
        {
            document?.Open();
        }

        void SaveButtonClick(object sender, RoutedEventArgs e)
        {
            document?.Save();
        }

        void SaveAsButtonClick(object sender, RoutedEventArgs e)
        {
            document?.SaveAs();
        }

        void PrintButtonClick(object sender, RoutedEventArgs e)
        {
            document?.Print();
        }

        void CloseButtonClick(object sender, RoutedEventArgs e)
        {
            document?.Close();
        }

        void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            var pressed = (RadioButton) sender;
            fileType = pressed.Content?.ToString();

            document = fileType switch
            {
                "JPEG" => factory.CreateJpegDocument(),
                "PNG" => factory.CreatePngDocument(),
                "Txt" => factory.CreateTextDocument(),
                _ => null
            }; //ReSharper
        }
    }
}
