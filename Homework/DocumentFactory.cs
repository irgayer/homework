﻿using System;
using System.Collections.Generic;
using System.Text;
using Homework.Interfaces;
using Homework.Models;

namespace Homework
{
    public class DocumentFactory
    {
        public IDocument CreateJpegDocument()
        {
            return new JpegDocument();
        }

        public IDocument CreatePngDocument()
        {
            return new PngDocument();
        }

        public IDocument CreateTextDocument()
        {
            return new TextDocument();
        }
    }
}
