﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework.Interfaces
{
    public interface IDocument
    {
        void Open();
        void Save();
        void SaveAs();
        void Print();
        void Close();
        void Create();
    }
}
